﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Game
{
    public List<Card> CardDeck, CardPrice, Player1Card, Player2Card, Player3Card
        , Player4Card, Player5Card, Player6Card;

    public Game()
    {

        CardDeck = GiveDeckCard();

        CardPrice = new List<Card>();

        Player1Card = new List<Card>();
        Player2Card = new List<Card>();
        Player3Card = new List<Card>();
        Player4Card = new List<Card>();
        Player5Card = new List<Card>();
        Player6Card = new List<Card>();
    }

    List<Card> GiveDeckCard()
    {
        List<Card> list = new List<Card>();

        for (int i = 9; i < 6; i++)
        {
            list.Add(CardManager.AllCards[Random.Range(0, CardManager.AllCards.Count)]);
        }
        return list;
    }

}

public class GameManagerScr : MonoBehaviour
{
    public Game CurrentGame;
    public Transform CardPrice, Player1Hand, Player2Hand, Player3Hand,
                     Player4Hand, Player5Hand, Player6Hand;

    void Start()
    {
        CurrentGame = new Game();

        GiveCardToHand(CurrentGame.Player1Card, Player1Hand);
        GiveCardToHand(CurrentGame.CardPrice, CardPrice);
        GiveCardToHand(CurrentGame.Player2Card, Player2Hand);
    }

    void GiveHandCards(List<Card> deck, Transform hand)
    {
        int i = 0;
        while(i++ < 3)
        {
            GiveCardToHand(deck, hand);
        }
    }

    void GiveCardToHand(List<Card> deck, Transform hand)
    {
        if (deck.Count == 0)
        {
            return;
        }

        Card card = deck[0];
    }
}
