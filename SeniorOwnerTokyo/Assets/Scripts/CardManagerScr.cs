﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Card
{
    public string Name;
    public Sprite Logo;
    public int Price, ActionOnMove, Act1, Act2;

    public bool Permoment;

    public Card(string name, string logoPath, int price, int actionMove, int act1, int act2, bool permoment)
    {
        Name = name;
        Logo = Resources.Load<Sprite>(logoPath);
        Price = price;
        ActionOnMove = actionMove;
        Act1 = act1;
        Act2 = act2;
        Permoment = permoment;
    }
}

public static class CardManager
{
    public static List<Card> AllCards = new List<Card>();
}

public class CardManagerScr : MonoBehaviour
{

    public void Awake()
    {
        CardManager.AllCards.Add(new Card("+ 1 Winpoint", "Sprites/Cards/Card1", 3, 1, 1, 0, false));
        CardManager.AllCards.Add(new Card("+ 2 Winpoint and Health 3HP", "Sprites/Cards/Card2", 6, 1, 2, 3, false));
        CardManager.AllCards.Add(new Card("+ 2 Winpoint", "Sprites/Cards/Card3", 4, 1, 2, 0, false));
        CardManager.AllCards.Add(new Card("+ 3 Winpoint", "Sprites/Cards/Card4", 5, 1, 3, 0, false));
        CardManager.AllCards.Add(new Card("+ 4 Winpoint", "Sprites/Cards/Card5", 6, 1, 4, 0, false));
        CardManager.AllCards.Add(new Card("+ 9 Energy", "Sprites/Cards/Card6", 8, 1, 9, 0, false));
    }

}
