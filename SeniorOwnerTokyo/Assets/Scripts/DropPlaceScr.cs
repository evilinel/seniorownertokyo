﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public enum FieldType
{
    PLAYER1_HAND,
    PLAYER2_HAND,
    PLAYER3_HAND,
    PLAYER4_HAND,
    PLAYER5_HAND,
    PLAYER6_HAND,
    PRICE_FIELD
}

public class DropPlaceScr : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
    public FieldType Type;

    public void OnDrop(PointerEventData eventData)
    {
        if (Type != FieldType.PLAYER1_HAND)
        {
            return;
        }

        CardMovementSrc card = eventData.pointerDrag.GetComponent<CardMovementSrc>();

        if (card)
        {
            card.DefaultPerent = transform;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (eventData.pointerDrag == null || Type != FieldType.PLAYER1_HAND)
        {
            return;
        }

        CardMovementSrc card = eventData.pointerDrag.GetComponent<CardMovementSrc>();

        if (card)
        {
            card.DefaultTempCardParent = transform;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (eventData.pointerDrag == null)
        {
            return;
        }

        CardMovementSrc card = eventData.pointerDrag.GetComponent<CardMovementSrc>();

        if (card && card.DefaultTempCardParent == transform)
        {
            card.DefaultTempCardParent = card.DefaultPerent;
        }
    }
}
